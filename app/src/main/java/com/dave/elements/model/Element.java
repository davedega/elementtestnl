package com.dave.elements.model;

/**
 * Created by davedega on 14/12/16.
 */

public class Element {
    public String title;
    public String description;
    public String urlImage;

    public Element(String title, String description, String urlImage) {
        this.title = title;
        this.description = description;
        this.urlImage = urlImage;
    }
}
