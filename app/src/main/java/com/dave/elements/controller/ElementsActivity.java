package com.dave.elements.controller;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.dave.elements.R;
import com.dave.elements.model.Element;
import com.dave.elements.utils.InputStreamVolleyRequest;
import com.dave.elements.viewmodel.IElementsViewModel;
import com.dave.elements.viewmodel.IElementsViewModelListener;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by davedega on 14/12/16.
 */

public class ElementsActivity extends AppCompatActivity implements IElementsViewModelListener,
        Response.Listener<byte[]>, Response.ErrorListener {

    private static final int REQUEST_PERMISSIONS = 200;
    private static final String TAG = "ElementsActivity";

    private Context mContext;
    private IElementsViewModel viewModel;
    InputStreamVolleyRequest request;


    ArrayList<Element> mElements;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = getApplicationContext();
        viewModel = (IElementsViewModel) getSupportFragmentManager().findFragmentById(R.id.fragment);
        viewModel.setListener(this);
        requestMMPermissions();
        downloadData();
    }


    private void requestMMPermissions() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            String writeStoragePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            String readStoragePermission = Manifest.permission.READ_EXTERNAL_STORAGE;


            int hasWritePermission = checkSelfPermission(writeStoragePermission);
            int hasReadPermission = checkSelfPermission(readStoragePermission);

            List<String> permissions = new ArrayList<String>();

            if (hasWritePermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(writeStoragePermission);
            }
            if (hasReadPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(readStoragePermission);
            }
            if (!permissions.isEmpty()) {
                final String[] params = permissions.toArray(new String[permissions.size()]);
                requestPermissions(params, REQUEST_PERMISSIONS);

            }
        }
    }

    @Override
    public void downloadData() {
        String mUrl = "https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv";
        request = new InputStreamVolleyRequest(Request.Method.GET, mUrl, ElementsActivity.this, ElementsActivity.this, null);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext(),
                new HurlStack());
        mRequestQueue.add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(error.getCause() instanceof UnknownHostException){
            Toast.makeText(mContext, "Unable to connect to Internet :(", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponse(byte[] response) {
        Log.d(TAG, "onResponse!");
        int count;
        mElements = new ArrayList<>();

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            if (response != null) {


                try {
                    long lenghtOfFile = response.length;
                    String filename = "sipliswewert.csv";

                    //covert reponse to input stream
                    InputStream input = new ByteArrayInputStream(response);
                    File path = Environment.getExternalStorageDirectory();
                    File file = new File(path, filename);
                    BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                    byte data[] = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = br.readLine()) != null) {

                        if (line.contains(",")) {
                            String[] entries = line.split(",");
                            if (entries.length == 3) {
                                Element newelement = new Element(entries[0], entries[1], entries[2]);
                                mElements.add(newelement);
                            }
                        }
//                        else {
//                            stringBuilder.append(line);
//                        }
                    }
                    viewModel.populateListView(mElements);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exc" + e.getMessage());
            e.printStackTrace();
        }
    }
}
