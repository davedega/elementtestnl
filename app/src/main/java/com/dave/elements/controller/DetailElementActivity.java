package com.dave.elements.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dave.elements.R;
import com.dave.elements.model.Element;
import com.dave.elements.viewmodel.IDetailElementViewModel;

/**
 * Created by davedega on 14/12/16.
 */

public class DetailElementActivity extends AppCompatActivity {
    private Context mContext;
    IDetailElementViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_detail_element);
        mContext = getApplicationContext();
        viewModel = (IDetailElementViewModel) getSupportFragmentManager().findFragmentById(R.id.fragment);
        viewModel.showDetail(new Element(
                getIntent().getStringExtra("title"),
                getIntent().getStringExtra("description"),
                getIntent().getStringExtra("image")));
    }
}
