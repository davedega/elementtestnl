package com.dave.elements.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dave.elements.R;
import com.dave.elements.controller.DetailElementActivity;
import com.dave.elements.model.Element;
import com.dave.elements.viewmodel.IElementsViewModel;
import com.dave.elements.viewmodel.IElementsViewModelListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davedega on 14/12/16.
 */

public class ElementsFragment extends Fragment implements IElementsViewModel {
    View rootView;
    ListView listView;
    Context _ctx;
    String TAG = "ElementsFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        _ctx = getActivity().getApplicationContext();
        rootView = inflater.inflate(R.layout.fragment_element, container, false);
        listView = (ListView) rootView.findViewById(R.id.listview);
        return rootView;
    }

    @Override
    public void setListener(IElementsViewModelListener mListener) {

    }

    @Override
    public void populateListView(final ArrayList<Element> mElements) {
        ElementsAdapter adapter = new ElementsAdapter(_ctx,
                R.layout.element_listview_item, mElements);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(getActivity(), DetailElementActivity.class);

                intent.putExtra("title", mElements.get(i).title);
                intent.putExtra("description", mElements.get(i).description);
                intent.putExtra("image", mElements.get(i).urlImage);

                startActivity(intent);
            }
        });

    }

    class ElementsAdapter extends ArrayAdapter<Element> {

        TextView title, description;
        ImageView imageView;

        Context _ctx;
        int resource;

        public ElementsAdapter(Context context, int resource,
                               List<Element> objects) {
            super(context, resource, objects);
            _ctx = context;
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Override
        public Element getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) this.getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.element_listview_item, parent,
                        false);
            }
            title = (TextView) row.findViewById(R.id.title);
            description = (TextView) row.findViewById(R.id.description);
            imageView = (ImageView) row.findViewById(R.id.element_image);

            final Element element = getItem(position);

            title.setText(element.title);
            description.setText(element.description);
            Log.i("URL: ", element.urlImage);
            Glide.with(_ctx)
                    .load(element.urlImage)
                    .skipMemoryCache(false)
                    .placeholder(R.drawable.placeholder)
                    .into(imageView);

            return row;
        }
    }
}
