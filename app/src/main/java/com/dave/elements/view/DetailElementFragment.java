package com.dave.elements.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dave.elements.R;
import com.dave.elements.model.Element;
import com.dave.elements.viewmodel.IDetailElementViewModel;

/**
 * Created by davedega on 14/12/16.
 */

public class DetailElementFragment extends Fragment implements IDetailElementViewModel {

    View rootView;
    TextView title, description;
    ImageView imageView;
    Context _ctx;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        _ctx = getActivity().getApplicationContext();
        rootView = inflater.inflate(R.layout.fragment_detail_element, container, false);
        title = (TextView) rootView.findViewById(R.id.title);
        description = (TextView) rootView.findViewById(R.id.description);
        imageView = (ImageView) rootView.findViewById(R.id.element_image);
        return rootView;
    }

    @Override
    public void showDetail(Element mElement) {
        Log.i("Image selected: ", ""+mElement.urlImage);
        title.setText(mElement.title);
        description.setText(mElement.description);
        Glide.with(_ctx)
                .load(mElement.urlImage)
                .skipMemoryCache(false)
                .placeholder(R.drawable.placeholder)
                .into(imageView);

    }
}
