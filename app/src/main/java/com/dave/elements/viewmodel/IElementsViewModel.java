package com.dave.elements.viewmodel;

import com.dave.elements.model.Element;

import java.util.ArrayList;

/**
 * Created by davedega on 14/12/16.
 */

public interface IElementsViewModel {
    void setListener(IElementsViewModelListener mListener);

    void populateListView(ArrayList<Element> mElements);
}
