package com.dave.elements.viewmodel;

import com.dave.elements.model.Element;

/**
 * Created by davedega on 14/12/16.
 */

public interface IDetailElementViewModel {
    void showDetail(Element mElement);
}
