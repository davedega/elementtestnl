package com.dave.elements.viewmodel;

/**
 * Created by davedega on 14/12/16.
 */

public interface IElementsViewModelListener {
    void downloadData();

}
